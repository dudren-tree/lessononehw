function ride () {
    return console.log('Поезд ' + this.name + ' везет '  + this.passNumb + ' пассажиров со скоростью ' + this.trainSpeed + 'км в час');
}
function stop () {
    return console.log('Поезд ' + this.name + ' остановился. ' + 'Скорость ' + this.trainSpeed * 0 + 'км в час');
}
function takePass() {
    let y = +prompt('Введите сколько подобрали пассажиров ');
    this.passNumb += y;
    return console.log('Подобрано ' + y + ' пассажиров. ' + ' Количество пассажиров: ' + this.passNumb);
}
let funOne = document.getElementById('ride');
let funTwo = document.getElementById('stop');
let funThr = document.getElementById('take');
let train = {
    name:'Hundai',
    trainSpeed:180,
    passNumb:200
}
let trainRide = ride.bind(train);
let trainStop = stop.bind(train);
let add = takePass.bind(train);
// train.stop();
// train.takePass(5);
funOne.addEventListener('click', trainRide);
funTwo.addEventListener('click', trainStop);
funThr.addEventListener('click', add);
/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
 