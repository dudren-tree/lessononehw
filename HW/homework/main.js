var all;
var back = document.getElementById('app');
  back.style.display = 'flex';
  back.style.justifyContent = 'center';
var backGround = document.createElement ('div');
  backGround.style.height = '100px';
  backGround.style.width = '100px';
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  var b = Math.floor(Math.random() * (max - min + 1)) + min;
  var str = b.toString(16);
  console.log (str);
  b = Math.floor(Math.random() * (max - min + 1)) + min;
  var strSec = b.toString(16);
  console.log (strSec);
  b = Math.floor(Math.random() * (max - min + 1)) + min;
  var strThi = b.toString(16);
  console.log (strThi);
  all = str + strSec + strThi;
  return all;
}
function getRandBack (a){
  console.log (a);
  var c = '#' + a;
  backGround.style.background = c;
}
backGround.onclick = function () {
  var c = '#' + getRandomIntInclusive(0, 15);
  backGround.style.background = c;
}
getRandomIntInclusive(0, 15);
getRandBack(all);
back.appendChild (backGround);

/*Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb */